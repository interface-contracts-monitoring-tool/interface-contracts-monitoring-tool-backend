import requests
import logging
import logging.config

# Logging configuration
logging.config.fileConfig("conf/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("main")

def getAuthToken(apiSGUrl:str, clientSecret:str, tokenUser:str, tokenPass:str):
    """ Returns string containing Bearer Token obtained from API security gateway
    """
    # Define token request parameters
    url = f"{apiSGUrl}/auth/realms/efpf/protocol/openid-connect/token"
    body = {
        "grant_type": "password",
        "client_id": "apisix",
        "client_secret": clientSecret,
        "username": tokenUser,
        "password": tokenPass
    }
    headers ={
        "Content-Type": "application/x-www-form-urlencoded"
    }
    logger.info(url)
    logger.info(body)
    try:
        r = requests.post(url=url, headers=headers, data=body)
        token = r.json()["access_token"]
        return token
    except:
        print("Failed to obtain authorization token")
        exit(1)
