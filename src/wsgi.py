import databases
import sqlalchemy
import uvicorn
import argparse
from fastapi import FastAPI
from pydantic import BaseModel
import logging
from typing import List, Optional
from fastapi.responses import ORJSONResponse


# Logging configuration
logging.config.fileConfig("conf/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("main")


# Parser configuration
parser = argparse.ArgumentParser()
parser.add_argument("--host", help="host the backend is binded to", type=str)
parser.add_argument("--port", help="port the backend is running on", type=int)
args = parser.parse_args()


# SQLAlchemy specific code, as with any other app
DATABASE_URL = "sqlite:///./data/database.db"
database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()

ic_table = sqlalchemy.Table(
    "inter_contract",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.String, primary_key=True),
    sqlalchemy.Column("tool_id", sqlalchemy.String),
    sqlalchemy.Column("api_id", sqlalchemy.String),
    sqlalchemy.Column("tool", sqlalchemy.String),
    sqlalchemy.Column("tool_desc", sqlalchemy.String),
    sqlalchemy.Column("api_desc", sqlalchemy.String),
    sqlalchemy.Column("owner", sqlalchemy.String),
    sqlalchemy.Column("email", sqlalchemy.String),
    sqlalchemy.Column("created", sqlalchemy.String),
    sqlalchemy.Column("updated", sqlalchemy.String),
    sqlalchemy.Column("expires", sqlalchemy.String),
    sqlalchemy.Column("last_upd", sqlalchemy.String),
    sqlalchemy.Column("type_upd", sqlalchemy.String),
    sqlalchemy.Column("api_version", sqlalchemy.String),
    sqlalchemy.Column("api_type", sqlalchemy.String),
    sqlalchemy.Column("doc", sqlalchemy.String),
)

docs_table = sqlalchemy.Table(
    "docs",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.String, primary_key=True),
    sqlalchemy.Column("type", sqlalchemy.String),
    sqlalchemy.Column("version", sqlalchemy.String),
    sqlalchemy.Column("url", sqlalchemy.String),
    sqlalchemy.Column("content", sqlalchemy.String),
    sqlalchemy.Column("old_content", sqlalchemy.String),
)

engine = sqlalchemy.create_engine(
    DATABASE_URL, connect_args={"check_same_thread": False}
)

metadata.create_all(engine,checkfirst=True)


class IC(BaseModel):
    id: str
    tool_id: Optional[str]
    api_id: Optional[str]
    tool: Optional[str]
    tool_desc: Optional[str]
    api_desc: Optional[str]
    owner: Optional[str]
    email: Optional[str]
    created: Optional[str]
    updated: Optional[str]
    expires: Optional[str]
    last_upd: Optional[str]
    type_upd: Optional[str]
    api_version: Optional[str]
    api_type: Optional[str]
    doc: Optional[str]


class Docs(BaseModel):
    id: str
    type: Optional[str]
    version: Optional[str]
    url: Optional[str]
    content: Optional[str]
    old_content: Optional[str]

class Count(BaseModel):
    total: int


app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.get("/list/ic/count", response_model=Count, response_class=ORJSONResponse)
async def getAllIC():
    query = ic_table.select()
    data = await database.fetch_all(query)
    total = len(data)
    return Count.parse_obj({
        "total": total
    })

@app.get("/list/ic/", response_model=List[IC], response_class=ORJSONResponse)
async def getAllIC(page: int = 0):
    PER_PAGE = 15
    query = ic_table.select().order_by(ic_table.c.updated)
    data = await database.fetch_all(query)
    data.reverse()
    return data[(page-1)*PER_PAGE:(page)*PER_PAGE]

@app.get("/list/doc/", response_model=List[Docs], response_class=ORJSONResponse)
async def getAllIC():
    query = docs_table.select()
    return await database.fetch_all(query)

@app.get("/ic/", response_model=IC, response_class=ORJSONResponse)
async def getAllIC(id: str):
    query = ic_table.select().where(ic_table.c.id==id)
    return await database.fetch_one(query)

@app.get("/doc/", response_model=Docs, response_class=ORJSONResponse)
async def getAllDoc(id: str):
    query = docs_table.select().where(docs_table.c.id==id)
    return await database.fetch_one(query)


def run(host:str, port:int):
    logger.info(f"Starting up server on {host}:{port}")
    uvicorn.run("wsgi:app", host=host, port=port, root_path="", reload=False)

if __name__ == "__main__":
    run(args.host, args.port)
