from auth import getAuthToken
import requests
import traceback
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker
import logging
import logging.config
from models import IC_table
from ic import icFactory, docFactory
from models.models import Docs_table


# Logging configuration
logging.config.fileConfig("conf/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("main")


def getSR(SRUrl:str, apiSGUrl:str, clientSecret:str, tokenUser:str, tokenPass:str):
    authToken = getAuthToken(apiSGUrl, clientSecret, tokenUser, tokenPass)
    try:
        r = requests.get(
            url=SRUrl,
            headers={
                "Authorization": f"Bearer {authToken}",
                "Content-Type": "application/json"
            }
        )
        if r.ok:
            return r.json()
        else:
            return None
    except:
        return None

def compareVersion(old, new, oldLabel):

    oldSplit = old.split(".")
    newSplit = new.split(".")
    oldSplit = [int(x) for x in oldSplit]
    newSplit = [int(x) for x in newSplit]

    # SemVer uses the X.Y.Z format, by splitting on the '.' we get a 3 el array
    if len(oldSplit) == 3 and len(newSplit) == 3:
        if newSplit[0] > oldSplit[0]:
            return 'major'
        if newSplit[1] > oldSplit[1]:
            return 'minor'
        if newSplit[2] > oldSplit[2]:
            return 'patch'
        return oldLabel
    else:
        return 'unknown'


def checkForUpdates(service, engine):
    for api in service["apis"]:
        try:
            # {"id":"prova2", "description":"prova", "type":"prova", "url":"prova", "old_content":"prova"}
            Session = sessionmaker(bind=engine)
            session = Session()
            icID = f"{service['id']}-{api['id']}"

            # Check if the IC with key icID is already in the DB
            if session.query(Docs_table.id).filter(Docs_table.id==icID).count() < 1:
                # Create new IC and Doc objects
                print(f"NEW Processing service: {service['title']} - {api['title']}")
                doc = docFactory(icID, api)
                ic = icFactory(icID, api, service, doc.version, doc.type, new=True)

                # Store the new IC and Doc in the DB
                session.add(IC_table(**ic.__dict__))
                session.add(Docs_table(**doc.__dict__))
            else:
                # Fetch the corresponding Doc from the DB
                oldDoc = session.query(Docs_table).filter(Docs_table.id==icID).one()
                oldIC = session.query(IC_table).filter(IC_table.id==icID).one()

                # Create a new Doc from the API and compare the version
                doc = docFactory(icID, api)
                if doc.version == oldDoc.version:
                    # If the version matches then discard the new Doc,
                    newIC = icFactory(icID, api, service, doc.version, doc.type, new=False, type_upd=oldIC.type_upd)
                    session.query(IC_table).filter(IC_table.id==icID).update(newIC.__dict__, synchronize_session=False)
                else:
                    # else create store the old Doc
                    # content in the new Doc as oldContent and create a new IC

                    # Handle major/minor/patch update type
                    type_upd = compareVersion(oldDoc.version, doc.version, oldIC.type_upd)

                    # Update the IC and the Doc
                    doc.old_content = oldDoc.content
                    newIC = icFactory(icID, api, service, doc.version, doc.type, new=False, type_upd=type_upd)
                    session.query(Docs_table).filter(Docs_table.id==icID).update(doc.__dict__, synchronize_session=False)
                    session.query(IC_table).filter(IC_table.id==icID).update(newIC.__dict__, synchronize_session=False)

                    print(f"UPDATE Processing service: {service['title']} - {api['title']}")
            session.commit()
        except Exception as e:
            print(f"FAIL '{service['title']} - {api['title']}' due to '{traceback.print_exc()}'")

def deleteRemovedServices(sr, engine):
        try:
            Session = sessionmaker(bind=engine)
            session = Session()
            dbICids = [ic[0] for ic in session.query(Docs_table.id).all()]
            srICids = []
            for service in sr["services"]:
                for api in service["apis"]:
                    srICids.append(f"{service['id']}-{api['id']}")

            for dbICid in dbICids:
                if dbICid not in srICids:
                    print(f"Deleted {dbICid}")
                    session.delete(session.query(IC_table).filter(IC_table.id==dbICid).first())
                    session.delete(session.query(Docs_table).filter(Docs_table.id==dbICid).first())
            session.commit()

        except Exception as e:
            print(f"Failed deleting removed services {traceback.print_exc()}")
            #for api in service["apis"]:
            # Check if the IC with key icID is already in the DB

def loadSR(engine: Engine, apiSGUrl:str, clientSecret:str, tokenUser:str, tokenPass:str):
    logger.info("Loading SR")
    logger.info(f"{apiSGUrl}/apis/sr/")
    sr = getSR(f"{apiSGUrl}/apis/sr/", apiSGUrl, clientSecret, tokenUser, tokenPass)
    deleteRemovedServices(sr, engine)
    for service in sr["services"]:
        checkForUpdates(service, engine)

    logger.info("Done loading Service Registry to DB")