import argparse
import logging
import logging.config
import sqlalchemy
from serviceregistry import loadSR
from models import Base
import time

# Logging configuration
logging.config.fileConfig("conf/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("main")


# Parser configuration
parser = argparse.ArgumentParser()
parser.add_argument("--API_SG_url", help="port the backend is running on", type=str)
parser.add_argument("--client_secret", help="port the backend is running on", type=str)
parser.add_argument("--token_user", help="port the backend is running on", type=str)
parser.add_argument("--token_pass", help="port the backend is running on", type=str)

args = parser.parse_args()


# SQLAlchemy specific code, as with any other app
DATABASE_URL = "sqlite:///./data/database.db"

engine = sqlalchemy.create_engine(
    DATABASE_URL, connect_args={"check_same_thread": False}, echo=False
)

Base.metadata.create_all(engine)

if __name__ == "__main__":
    while True:
        logger.info("Updating")
        try:
            loadSR(engine, args.API_SG_url, args.client_secret, args.token_user, args.token_pass)
        except:
            logger.error("Couldn't load SR")
        time.sleep(60*2)