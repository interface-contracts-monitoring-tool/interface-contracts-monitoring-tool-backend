import requests
import json
import yaml
import datetime

class IC(object):
    def __init__(self, id, tool_id, api_id, tool, tool_desc, api_desc, owner, email, created, updated, expires, last_upd, type_upd, api_version, api_type, doc):
        self.id = id
        self.tool_id = tool_id
        self.api_id = api_id
        self.tool = tool
        self.tool_desc = tool_desc
        self.api_desc = api_desc
        self.owner = owner
        self.email = email
        self.created = created
        self.updated = updated
        self.expires = expires
        self.last_upd = last_upd
        self.type_upd = type_upd
        self.api_version = api_version
        self.api_type = api_type
        self.doc = doc

def icFactory(id, api, service, api_version, api_type, new, type_upd=None):
    if "owner" in service['meta']:
        owner = service['meta']['owner']
    else:
        owner = None

    if "email" in service['meta']:
        email = service['meta']['email']
    else:
        email = None

    if service['doc'] == "":
        doc = None
    else:
        doc = service['doc']

    if new:
        if api_version is None:
            type_upd = "unknown"
        else:
            type_upd = "new"

    ic = IC(
        id=id,
        tool_id=service['id'],
        api_id=api['id'],
        tool=f"{service['title']} [{api['title']}]",
        tool_desc=service['description'],
        api_desc=api['description'],
        owner=owner,
        email=email,
        created=service['createdAt'],
        updated=service['updatedAt'],
        expires=service['expiresAt'],
        last_upd=service['updatedAt'],
        type_upd=type_upd,
        api_version=api_version,
        api_type=api_type,
        doc=doc
    )
    return ic


class Doc(object):

    def __init__(self, id, type, version, url, content, old_content):
        self.id = id
        self.type = type
        self.version = version
        self.url = url
        self.content = content
        self.old_content = old_content

def docFactory(id, api):
    type = None
    content = None
    version = None
    url = None

    def getApiVersion(api):
        content = None
        version = None
        url = None
        if 'version' in api['spec']['schema']:
            version = api['spec']['schema']['version']
        if 'info' in api['spec']['schema']:
            if 'version' in api['spec']['schema']['info']:
                version = api['spec']['schema']['info']['version']
                content = json.dumps(api['spec']['schema'])
        elif api['spec']['url'].lower().split('.')[-1] == 'json':
            url = api['spec']['url']
            try:
                r= requests.get(api['spec']['url'])
                if r.ok:
                    spec = r.json()
                    version = spec['info']['version']
                    content = json.dumps(spec)
                else:
                    print(f"Failed to get API url [id={api['id']}]")
                    version = None
            except Exception as e:
                print(f"Failed getting API schema [id={api['id']}] {e}")
        elif api['spec']['url'].lower().split('.')[-1] == 'yaml':
            url = api['spec']['url']
            try:
                r= requests.get(api['spec']['url'])
                if r.ok:
                    spec = yaml.safe_load(r.text)
                    version = spec['info']['version']
                else:
                    print(f"Failed to get API url [id={api['id']}]")
                    version = None
            except Exception as e:
                print(f"Failed getting API schema [id={api['id']}] {e}")
        return content, version, url


    if "openapi" in api['spec']['mediaType']:
        if "version=3" in api['spec']['mediaType']:
            content, version, url = getApiVersion(api)
            type = "openapiv3"
        elif "version=2" in api['spec']['mediaType']:
            content, version, url = getApiVersion(api)
            type = "openapiv2"
    if "asyncapi" in api['spec']['mediaType']:
        if "version=2" in api['spec']['mediaType']:
            content, version, url = getApiVersion(api)
            type = "asyncapiv2"

    doc = Doc(
        id,
        type,
        version,
        url,
        content,
        None
    )

    return doc
