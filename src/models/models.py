from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String


Base = declarative_base()

class Docs_table(Base):
    __tablename__ = 'docs'
    id = Column(String, primary_key=True)
    type = Column(String)
    version = Column(String)
    url = Column(String)
    content = Column(String)
    old_content = Column(String)

class IC_table(Base):
    __tablename__ = 'inter_contract'
    id = Column(String, primary_key=True)
    tool_id = Column(String)
    api_id = Column(String)
    tool = Column(String)
    tool_desc = Column(String)
    api_desc = Column(String)
    owner = Column(String)
    email = Column(String)
    created = Column(String)
    updated = Column(String)
    expires = Column(String)
    last_upd = Column(String)
    type_upd = Column(String)
    api_version = Column(String)
    api_type = Column(String)
    doc = Column(String)